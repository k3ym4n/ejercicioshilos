package usuthreads;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/**
 * Created by k3ym4n on 16/12/2015.
 */
public class UsoThreads implements Runnable {


    private String nombre;

    //constructor

    public UsoThreads(String nombre){
        this.nombre=nombre;
    } /*esto es u ejemplo basico de hilo
     creo el objeto UsoThreads le meto el metodo run
     para que corra y le he a�adido un numero randon aun sleep*/

    public void run(){

        int x = (int) (Math.random()*5000);
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            System.out.println("Salto la excepcion");
            e.printStackTrace();
        }
        System.out.println("SOY " + nombre + "("+x+")");

    }



        public static void main(String[] args) {


            Thread T1 = new Thread(new UsoThreads("Pedro"));
            Thread T2 = new Thread(new UsoThreads("Manolo"));
            Thread T3 = new Thread (new UsoThreads("David"));


            T1.start();
            T2.start();
            T3.start();

        }



}









